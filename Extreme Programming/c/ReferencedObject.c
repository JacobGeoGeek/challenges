#include <stdlib.h>
#include <stdio.h>
#include "ReferencedObject.h"

struct ReferencedObject *ReferencedObject_new(void)
{
    struct ReferencedObject *pointer = malloc(sizeof(struct ReferencedObject));
    pointer->reference_count = 1;
    return pointer;
}

void ReferencedObject_reference(struct ReferencedObject *referencedObject)
{
    ++referencedObject->reference_count;
}

_Bool ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(struct ReferencedObject *referencedObject)
{
    --referencedObject->reference_count;

    if(referencedObject->reference_count == 0) {
        free(referencedObject);
        return 1;
    }

    return 0;
}
